# Deploiement de YASMINE

dépôt initial chez ISTI : https://gitlab.isti.com/resif/yasmine

Prérequis : 

* Accès en lecture au projet yasmine chez ISTI
* Accès en écriture au projet yasmine-k8s chez gricad-gitlab

## Construire et publier les images Docker
    git clone git@gitlab.isti.com:resif/yasmine.git
    cd yasmine
    cd frontend
    docker build -t gricad-registry.univ-grenoble-alpes.fr/osug/resif/yasmine-k8s/yasmine-frontend:$(git rev-parse --short HEAD)
    docker push gricad-registry.univ-grenoble-alpes.fr/osug/resif/yasmine-k8s/yasmine-frontend:$(git rev-parse --short HEAD)
    cd ..
    cd backend
    docker build -t gricad-registry.univ-grenoble-alpes.fr/osug/resif/yasmine-k8s/yasmine-backend:$(git rev-parse --short HEAD)
    docker push gricad-registry.univ-grenoble-alpes.fr/osug/resif/yasmine-k8s/yasmine-backend:$(git rev-parse --short HEAD)
    cd ../.. 
    
## Déployer sur kubernetes

kubectl apply -f yasmine.yaml

